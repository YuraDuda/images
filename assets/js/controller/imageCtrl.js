/**
 * Created by yura on 19.04.17.
 */

app.controller('imageCtrl', function ($scope,$http,imageRepository,FileUploader) {

  $scope.imageType = {
    value : '4'
  };

  var uploader = $scope.uploader = new FileUploader();
  $scope.images = [];

  uploader.filters.push({
    name: 'imageFilter',
    fn: function(item /*{File|FileLikeObject}*/, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });


  imageRepository.allList().success(function (data) {
    $scope.images = data.images;
  });

  $scope.uploadImage = function (file) {

      indicatorstart("Saving....");
      var formData = new FormData();
      formData.append("imageType",$scope.imageType.value);
      formData.append('file', file._file);

      imageRepository.create(formData).success(function (data) {
        indicatorstop();
        location.reload();
      });

  };



});
