/**
 * Created by yura on 18.04.17.
 */


app.factory('imageRepository', function ($http) {
  return {
    create: function (obj) {
      return $http.post('/image/upload', obj, {
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      });
    },

    allList : function () {
      return $http.get('/image/allList');
    }
  }
});

