/**
 * Created by yura on 13.07.17.
 */

var fs = require('fs');
var gm = require('gm');
var uuid = require("node-uuid");


var Crop = function () {

  this.crop = function(filePath,saveFilePath,fileName,next) {

    gm(filePath+fileName).size(function(err, value){

      var n  = 1;

      var retreat = 20; // 20px

      var heightImage = value.height + 40;
      var widthImage = value.width + (n+1)*retreat;

      var partHeight = value.height;
      var partWidth = value.width/n;

      var objResult = {};

      objResult.image = uuid.v4() + '.jpg';
      objResult.arrayCropImages = [];

      gm(widthImage, heightImage, "#ffffff")
        .write(saveFilePath + objResult.image, function (err) {
          objResult.arrayCropImages.push(uuid.v4()+'.jpg');
          gm(filePath+fileName).crop(partWidth, partHeight, 0, 0)
            .write(saveFilePath+objResult.arrayCropImages[0], function (err) {

              gm().in('-page', '+0+0')
                .in(saveFilePath + objResult.image)
                .in('-page', '+20+20') // location of smallIcon.jpg is x,y -> 10, 20
                .in(saveFilePath+objResult.arrayCropImages[0])
                .mosaic()
                .write(saveFilePath + objResult.image, function (err) {
                  if (err) console.log(err);

                  next(objResult);

                });
            });
        });
    });

  }

  this.crop2 = function(filePath,saveFilePath,fileName,next) {
    gm(filePath+fileName).size(function(err, value){

      var n  = 2;

      var retreat = 20; // 20px

      var heightImage = value.height + (value.height/4)+40;
      var widthImage = value.width + (n+1)*retreat;

      var partHeight = value.height;
      var partWidth = value.width/n;

      var objResult = {};

      objResult.image = uuid.v4() + '.jpg';
      objResult.arrayCropImages = [];
      gm(widthImage, heightImage, "#ffffff")
        .write(saveFilePath + objResult.image, function (err) {
          objResult.arrayCropImages.push(uuid.v4()+'.jpg');
          gm(filePath+fileName).crop(partWidth, partHeight, 0, 0)
            .write(saveFilePath+objResult.arrayCropImages[0], function (err) {
              objResult.arrayCropImages.push(uuid.v4()+'.jpg');
              gm(filePath+fileName).crop(partWidth, partHeight, partWidth, 0)
                .write(saveFilePath+objResult.arrayCropImages[1], function (err) {
                  gm().in('-page', '+0+0')
                    .in(saveFilePath + objResult.image)
                    .in('-page', '+20+' + partHeight/n) // location of smallIcon.jpg is x,y -> 10, 20
                    .in(saveFilePath+objResult.arrayCropImages[0])
                    .in('-page', '+' + (partWidth + 2 * retreat) + '+'+partHeight/6) // location of smallIcon.jpg is x,y -> 10, 20
                    .in(saveFilePath+objResult.arrayCropImages[1])
                    .mosaic()
                    .write(saveFilePath + objResult.image, function (err) {
                      if (err) console.log(err);

                      next(objResult);
                    });
                });

            });
        });
    });
  }

  this.crop3 = function(filePath,saveFilePath,fileName,next) {
    gm(filePath+fileName).size(function(err, value){

      var n  = 3;

      var retreat = 20; // 20px

      var heightImage = value.height + (value.height/4)+40;
      var widthImage = value.width + (n+1)*retreat;

      var partHeight = value.height;
      var partWidth = value.width/n;

      var objResult = {};

      objResult.image = uuid.v4() + '.jpg';
      objResult.arrayCropImages = [];
      gm(widthImage, heightImage, "#ffffff")
        .write(saveFilePath + objResult.image, function (err) {
          objResult.arrayCropImages.push(uuid.v4()+'.jpg');
          gm(filePath+fileName).crop(partWidth, partHeight, 0, 0)
            .write(saveFilePath+objResult.arrayCropImages[0], function (err) {
              objResult.arrayCropImages.push(uuid.v4()+'.jpg');
              gm(filePath+fileName).crop(partWidth, partHeight, partWidth, 0)
                .write(saveFilePath+objResult.arrayCropImages[1], function (err) {
                  objResult.arrayCropImages.push(uuid.v4()+'.jpg');
                  gm(filePath+fileName).crop(partWidth, partHeight, 2*partWidth, 0)
                    .write(saveFilePath+objResult.arrayCropImages[2], function (err) {
                      gm().in('-page', '+0+0')
                        .in(saveFilePath + objResult.image)
                        .in('-page', '+20+' + partHeight/n) // location of smallIcon.jpg is x,y -> 10, 20
                        .in(saveFilePath+objResult.arrayCropImages[0])
                        .in('-page', '+' + (partWidth + 2 * retreat) + '+'+partHeight/6) // location of smallIcon.jpg is x,y -> 10, 20
                        .in(saveFilePath+objResult.arrayCropImages[1])
                        .in('-page', '+' + (2 * partWidth + 3 * retreat) + '+'+partHeight/12) // location of smallIcon.jpg is x,y -> 10, 20
                        .in(saveFilePath+objResult.arrayCropImages[2])
                        .mosaic()
                        .write(saveFilePath + objResult.image, function (err) {
                          if (err) console.log(err);

                          next(objResult);
                        });
                    });

                });
            });
        });
    });
  }

  this.crop4 = function(filePath,saveFilePath,fileName,next) {

    gm(filePath+fileName).size(function(err, value){

      var n  = 4;

      var retreat = 20; // 20px

      var heightImage = value.height + (value.height/4)+40;
      var widthImage = value.width + (n+1)*retreat;

      var partHeight = value.height;
      var partWidth = value.width/n;

      var objResult = {};

      objResult.image = uuid.v4() + '.jpg';
      objResult.arrayCropImages = [];
      gm(widthImage, heightImage, "#ffffff")
        .write(saveFilePath + objResult.image, function (err) {
          objResult.arrayCropImages.push(uuid.v4()+'.jpg');
          gm(filePath+fileName).crop(partWidth, partHeight, 0, 0)
            .write(saveFilePath+objResult.arrayCropImages[0], function (err) {
              objResult.arrayCropImages.push(uuid.v4()+'.jpg');
              gm(filePath+fileName).crop(partWidth, partHeight, partWidth, 0)
                .write(saveFilePath+objResult.arrayCropImages[1], function (err) {
                  objResult.arrayCropImages.push(uuid.v4()+'.jpg');
                  gm(filePath+fileName).crop(partWidth, partHeight, 2*partWidth, 0)
                    .write(saveFilePath+objResult.arrayCropImages[2], function (err) {
                      objResult.arrayCropImages.push(uuid.v4()+'.jpg');
                      gm(filePath+fileName).crop(partWidth, partHeight, 3*partWidth, 0)
                        .write(saveFilePath+objResult.arrayCropImages[3], function (err) {
                          gm().in('-page', '+0+0')
                            .in(saveFilePath + objResult.image)
                            .in('-page', '+20+' + partHeight/n) // location of smallIcon.jpg is x,y -> 10, 20
                            .in(saveFilePath+objResult.arrayCropImages[0])
                            .in('-page', '+' + (partWidth + 2 * retreat) + '+'+partHeight/6) // location of smallIcon.jpg is x,y -> 10, 20
                            .in(saveFilePath+objResult.arrayCropImages[1])
                            .in('-page', '+' + (2 * partWidth + 3 * retreat) + '+'+partHeight/12) // location of smallIcon.jpg is x,y -> 10, 20
                            .in(saveFilePath+objResult.arrayCropImages[2])
                            .in('-page', '+' + (3 * partWidth + 4 * retreat) + '+'+partHeight/24) // location of smallIcon.jpg is x,y -> 10, 20
                            .in(saveFilePath+objResult.arrayCropImages[3])
                            .mosaic()
                            .write(saveFilePath + objResult.image, function (err) {
                              if (err) console.log(err);

                              next(objResult);
                            });
                        });

                    });
                });
            });
        });
    });
  }
};


module.exports = function () {
  var instance = new Crop();
  return instance;
};

