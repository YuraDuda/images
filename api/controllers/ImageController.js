/**
 * Created by yura on 13.07.17.
 */

var fs = require('fs');
var gm = require('gm');
var path = require('path');
var uuid = require("node-uuid");
var cropModule = require("./crop");
var crop = new cropModule();


module.exports = {

  list: function (req, res) {
    res.view('image/list');
  },

  allList : function (req,res) {
    Image.find({}).exec(function (err, images) {
        if(err){
          return res.badRequest();
        }

        return res.ok({images : images})
    });
  },

  upload : function (req,res) {
     var imageType = req.param('imageType');
      var reqFile = req.file('file');
      console.log(imageType);

      if (reqFile._files.length != 0) {

        req.file('file').upload({dirname: '../../assets/images/smile_phot/'}, function whenDone(err, uploadedFiles) {
          if (err) {
            console.log(err);
            return;
          }

          // If no files were uploaded, respond with an error.
          if (uploadedFiles.length === 0) {
            console.log('No file was uploaded');
            return res.badRequest();
          }

          Image.create({name : uploadedFiles[0].fd.substring(uploadedFiles[0].fd.lastIndexOf(path.sep) + 1)}).exec(function (err, imageCreated) {
            if (err) {
              return res.badRequest();
            }

            selectType(imageType,'./assets/images/smile_phot/','./assets/images/smile_phot/',uploadedFiles[0].fd.substring(uploadedFiles[0].fd.lastIndexOf(path.sep) + 1),function (objResult) {
              Image.update({id : imageCreated.id},{generatedImageName : objResult.image, cropImagesArray : objResult.arrayCropImages}).exec(function (err,updated) {
                if(err){
                  return res.badRequest();
                }
                return res.ok();
              });
            });

          });

        });

      } else {
        return res.badRequest('Empty file list');
      }

  },

};

function selectType(type,filePath,saveFilePath,fileName,next) {

  switch (type) {
    case "1":
      crop.crop(filePath,saveFilePath,fileName,next);
      break;

    case "2":
      crop.crop2(filePath,saveFilePath,fileName,next);
      break;

    case "3":
      crop.crop3(filePath,saveFilePath,fileName,next);
      break;

    case "4":
      crop.crop3(filePath,saveFilePath,fileName,next);
      break;

    default:{
      crop.crop4(filePath,saveFilePath,fileName,next);
    }
  }
}


