/**
 * Created by yura on 13.07.17.
 */


var uuid = require("node-uuid");

module.exports = {
  connection: 'worldMongo',
  autoPK: false,
  schema: true,
  attributes: {
    id: {
      type: 'string',
      required: true,
      unique: true,
      primaryKey: true,
      defaultsTo: function () {
        return uuid.v4();
      }
    },

    name : {
      type: 'string'
    },

    generatedImageName : {
      type: 'string'
    },

    cropImagesArray: {
      type: 'array'
    }

  }
};
